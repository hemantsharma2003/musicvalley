<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//for cross request origin code
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, auth,role-id, Origin, Authorization');
//end for cross request origin code


Route::prefix('musicvalley/app')->namespace('Api')->group(function () {
    // Login
    Route::post('/login', 'AuthController@appLogin');
    Route::post('/register', 'AuthController@appRegister');
    Route::post('/otpverify', 'AuthController@OtpVerify');
    Route::post('/resendotp', 'AuthController@resendOtp');
    Route::post('/testotp', 'AuthController@testOtp');
    
    // Register
    // Protected with APIToken Middleware
    Route::middleware('APIToken')->group(function () {
        Route::post('/logout', 'AuthController@userLogout');
    });
});

Route::prefix('musicvalley/admin')->namespace('Admin')->group(function () {
    // Login
    Route::post('/userlogin', 'AuthController@userLogin');
    Route::post('/userregister', 'AuthController@userRegister');
    Route::post('/otpvarify', 'AuthController@OtpVarify');

    // Register
    // Protected with APIToken Middleware
    Route::middleware('APIToken')->group(function () {
        Route::post('/adminlogout', 'AuthController@adminLogout');
    });
});

