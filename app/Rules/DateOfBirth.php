<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DateOfBirth implements Rule
{
    protected $customMessage;
    protected $context;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($context)
    {
        $this->context = $context;
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($attribute == 'dob'){
            return $this->checkDateOfBirth($value);
        }
        
          if($attribute == 'mobile'){
            return $this->checkMobileNo($value);
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if(!empty($this->customMessage)){
            return $this->customMessage;
        }
        else{
            return 'Error!';
        }
        
    }
    
    public function checkDateOfBirth($value){
        if(!strtotime($value)){
            return false;
        }
        $dateDiff = strtotime(date('Y-m-d')) - strtotime($value);
        $minDay = 3*365;
        $maxDay = 100*365;
        
        if(empty($dateDiff)){
            $this->customMessage = 'Invalid Date';
            return false;
        }
        
        
        
        $dateDiffDays = floor($dateDiff/86400);
        
        if($dateDiffDays < $minDay|| $dateDiffDays >$maxDay){
            $this->customMessage = 'DOB should be above 3 yrs';
           return false;
        }
        
        return true;
        
    }
    
    public function checkMobileNo($value){
        $country = $this->context->phone_code;
        
        if($country == '91' && strlen($value)!= '10') {
            $this->customMessage = 'mobile no should be 10 digit';
            return false;
        }
        elseif($country == '965' && strlen($value)!='14'){
            $this->customMessage = 'mobile no should be 14 digit';
            return false;
        }
        return true;
    }
}
