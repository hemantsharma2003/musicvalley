<?php

namespace App\Http\Middleware;

use Closure;
// use App\Model\AdminUser;
// use App\User;

class APIToken {

    public $userData;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!empty($request->header('auth')) && !empty($request->header('role-id'))) {
            $token = trim($request->header('auth'));
            $roleId = trim($request->header('role-id'));
            $validRoldId = preg_match('/^\d+$/',$roleId);
            
            if($validRoldId && $roleId==1){
             $data = AdminUser::where(['token' => $token, 'status' => 1,'role_id' => $roleId])->first();
            }
            else if($validRoldId && ($roleId== 2 || $roleId== 3)){
                $data = User::where(['token' => $token, 'status' => 1,'role_id' => $roleId])->first();
            }
            
           if(!empty($data)) {
                return $next($request);
            }
        }
        $result['msg'] = "Not a valid API request.";
        $result['statuscode'] = 200;
        $result['success'] = false;
        $result['error'] = true;

        return response()->json($result);
    }

}
