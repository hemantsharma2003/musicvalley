<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use App\Rules;
use Illuminate\Support\Facades\Config;

class AuthController extends Controller
{
    private $apiToken;
    private $otp;

    public function __construct() {
        $this->apiToken = uniqid(base64_encode(Str::random(60)));
        $this->otp = mt_rand(1111, 9999);
    }

    public function userLogin(Request $request){
        echo "user login";
    }
}
