<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('mobile')->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('dob')->nullable();
            $table->integer('gender')->comment('1=>male, 2=>female,3=>other');
            $table->integer('country_code');
            $table->integer('language_id')->default(1)->comment('english language');
            $table->string('profile_image')->nullable();
            $table->string('token')->nullable();
            $table->integer('otp')->nullable();
            $table->tinyInteger('is_otp_verified')->default(0);
            $table->datetime('otp_verified_at')->nullable();
            $table->string('device_id')->nullable()->comment('will use when register');
            $table->string('device_token')->nullable()->comment('will use when notification');
            $table->integer('device_type')->comment('1=>android,2=>ios');
            $table->integer('role_id')->comment('1=>admin, 2=>user,3=>other');
            $table->tinyInteger('is_verified')->default(0)->comment('0 => inactive,1=> active');
            $table->tinyInteger('status')->default(1)->comment('0 => inactive,1=> active');
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
