<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('YOUR_DEFINED_CONST', 'Your defined constant value!');

return [
    'APP_INFO' => [
        'APP_NAME' => 'musicvalley',
        'APP_KEY' => 'xxxxx-xxxx',
        'APP_SECRET' => 'xxxxxxx-xxxxxx-xxxx'
    ],

    'LANGUAGE_TYPE' => [
        'ENG_LANGUAGE' => 1,
        'HINDI_LANGUAGE' => 3,
        'PUNJABI_LANGUAGE' => 2,
    ],
    
    'LANGUAGE_LIST' => [
        1 => 'en',
        2 => 'pa',
        3 => 'hi',
        4 => 'ar'
    ],

    'PAYMENT_STATUS' => [
        'INITIATE' => 0,
        'COMPLETE' => 1,
        'PENDING' => 2,
        'FAILED' => 3,
        'CANCEL' => 4,
        'RESCHEDULE' => 5,
    ],

    'STATUS_TYPE' => [
        'ACTIVE_STATUS' => 1,
        'INACTIVE_STATUS' => 0,
        'API_SUCCESS_STATUS' => 200,
        'API_BAD_STATUS' => 400,
        'API_PENDING_STATUS' => 1,
        'API_APPROVE_STATUS' => 1,
    ],

    'COUNTRY_DATA' => [
        'IND' => 91,
        'US' => 'www.domain.us',
        'UK' => 'www.domain.uk',
        'BR' => 'www.domain.br',
        'IT' => 'www.domain.it',
        'DE' => 'www.domain.de',
        'FR' => 'www.domain.fr'
    ],

    'ROLE_LIST' => [
        'SUPERADMIN' => 1,
        'USER' => 2,
    ],
    
    //multi dimensional associative array
    'TEST_DATA' => [
        'IN' => ['name' => 'india', 'shortname' => "IN", 'phone_code' => 91],
        'US' => ['name' => 'india', 'shortname' => "IN", 'phone_code' => 91],
        'UK' => ['name' => 'india', 'shortname' => "IN", 'phone_code' => 91],
        'BR' => ['name' => 'india', 'shortname' => "IN", 'phone_code' => 91],
    ],
    
    '_ENV' => [
        'APP_KEY',
        'DB_PASSWORD',
    ],
];
